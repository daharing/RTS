# RTS

## Info
This repo contains group 3's real time systems CpE 5170 code for all team assignments. It also contains, as a submodule, the original repo used (which is Dr. Z's repo).

## Tasks
When cloneing this repo, it is advised to use 
    git clone --recursive https://bitbucket.org/daharing/rts.git
or, after having cloned the repo, using
    git submodule update --init --recursive
to be sure that the embeded submodule gets populated. Please note that because it is a submodule, it will need to be updated independantly (git fetch && git pull) of the parent (this) repo. If there were changes in the submodule, you will need to make a commit for the parent repo for the changes to be held in our repo. Here, https://github.blog/2016-02-01-working-with-submodules/, is a link that gives some submodule descriptions and commands.

Upon cloning this repo, git checkout lab2 if you are working on the lab2 portion of the RTS class. Also, cd into original_repo and git checkout lab_1_hints_solutions.

